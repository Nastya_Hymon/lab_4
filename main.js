const addStudentModalWrapper = document.getElementById("add-student");
const confirmDeletionModalWrapper = document.getElementById("confirm-deletion");
const addStudentModal = addStudentModalWrapper?.querySelector(".modal");
const confirmDeletionModal = confirmDeletionModalWrapper?.querySelector(".modal");

const addStudentForm = document.getElementById("add-student-form");
const addStudentButton = document.getElementById("add-student-btn");
const closeModalButton = document.getElementById("close-modal-btn");
const studentsTable = document.getElementById("students-table");

let isFormVisible = false;

let isEditing = false;

let isAdding = false;

addStudentButton.onclick = () => {
  if (!isEditing && !isAdding) {
    showStudentModal();
    isFormVisible = true;
  } else {
    alert("Please finish editing or adding before adding a new student.");
  }
};

closeModalButton.onclick = function (e) {
  e.preventDefault();
  if (!isFormVisible) return;
  hideStudentModal();
  isFormVisible = false;
  isAdding = false;
};


async function fetchStudents() {
  try {
    const response = await fetch("fetch_students.php");
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    
    if (data.success) {
      const students = data.students;
      const tbody = studentsTable.querySelector("tbody");
      tbody.innerHTML = ""; 
      students.forEach((student) => {
        createStudent(student);
      });
    } else {
      throw new Error(data.message);
    }
  } catch (error) {
    console.error("Error fetching students:", error.message);
    alert("Failed to fetch students. Please try again later.");
  }
}


fetchStudents();


addStudentForm.addEventListener("submit", async function (event) {
  event.preventDefault();

  if (isEditing) {
    return; 
  }
  isAdding = true;
  const formData = new FormData(addStudentForm);

  const name = formData.get('name');
  const surname = formData.get('surname');
  const birthday = new Date(formData.get('birthday'));

  const nameValid = /^[A-Z][a-z]+$/.test(name);
  const surnameValid = /^[A-Z][a-z]+$/.test(surname);

  const currentDate = new Date();
  const age = currentDate.getFullYear() - birthday.getFullYear();
  const ageValid = age >= 16 && age <= 70;

  let errorMessage = "";
  if (!nameValid) errorMessage += "Name should start with an uppercase letter and consist of Latin letters.\n";
  if (!surnameValid) errorMessage += "Surname should start with an uppercase letter and consist of Latin letters.\n";
  if (!ageValid) errorMessage += "Age should be between 16 and 70 years.\n";

  try {
    const response = await fetch("server.php", {
      method: "POST",
      body: formData
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const data = await response.json();

    if (data.success) {

      alert(data.message);
      addStudentForm.reset();

      const student = {
        group: formData.get('group'),
        name: name,
        surname: surname,
        gender: formData.get('gender'),
        birthday: birthday,
        active: true
      };


      //students.push(student);
      //createStudent(student);
      hideStudentModal();
      fetchStudents();
      isAdding = false;
    } else {

      alert(errorMessage);
    }
  } catch (error) {
    console.error('Error:', error.message, formData);
    alert(errorMessage || 'An error occurred. Please try again later.');
  }
});


function createStudent({id, group, name, surname, birthday, gender, active}, replace) {
  const tr = document.createElement("tr");
  tr.id = `student-${id}`;
  const checkbox = document.createElement("input");
  const checkBoxTd = document.createElement("td");
  checkbox.type = "checkbox";
  checkBoxTd.appendChild(checkbox);
  tr.appendChild(checkBoxTd);

  const cells = [group, `${name} ${surname}`, gender, birthday];
 
  for (let cellContent of cells) {
    const td = document.createElement("td");
    td.textContent = cellContent;
    tr.appendChild(td);  
  }

  const activeTd = document.createElement("td");
  const activeDiv = document.createElement("div");
  activeDiv.classList.add("status");
  active && activeDiv.classList.add("active");
  activeTd.appendChild(activeDiv);
  tr.appendChild(activeTd);


  const optionsTd = document.createElement("td");
  const editButton = document.createElement("button");
  const removeButton = document.createElement("button");
  editButton.onclick = () => onStudentEditModal(id);
  //removeButton.onclick = () => confirmDeletion(id);
  removeButton.onclick = () => deleteStudent(id);
  const editButtonIcon = document.createElement("i");
  editButtonIcon.classList.add("fa", "fa-edit");
  const removeButtonIcon = document.createElement("i");
  removeButtonIcon.classList.add("fa", "fa-remove");

  editButton.appendChild(editButtonIcon);
  removeButton.appendChild(removeButtonIcon);

  optionsTd.appendChild(editButton);
  optionsTd.appendChild(removeButton);
  tr.appendChild(optionsTd);

  if (replace) {
    document.getElementById(`student-${id}`).replaceWith(tr);
  } else {
    studentsTable.querySelector("tbody").appendChild(tr);
  }
  return tr;
}


async function deleteStudent(id) {
  const confirmed = confirm("Are you sure you want to delete this student?");
  if (!confirmed) return;

  console.log('Deleting student with ID:', id);

  try {
    const response = await fetch(`server.php`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ id: id }) 
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const data = await response.json();

    if (data.success) {
      alert(data.message);
      const deletedStudent = document.getElementById(`student-${id}`);
      if (deletedStudent) {
        deletedStudent.remove(); 
      }
    } else {
      alert(data.message);
    }
  } catch (error) {
    console.error("Error deleting student:", error.message);
    alert("Failed to delete student. Please try again later.");
  }
}


async function updateStudent(id, formData) {
  try {
    formData.append('id', id);

    const response = await fetch("server.php", {
      method: "POST",
      body: formData
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const data = await response.json();

    if (data.success) {
      alert(data.message);
      hideStudentModal();
      fetchStudents(); 
      isEditing = false;
      addStudentForm.reset(); 
      const modalTitle = document.getElementById("modal-title");
      modalTitle.textContent = "Add Student";
    } else {
      alert(errorMessage);
    }
   
  } catch (error) {
    console.error('Error:', error.message, formData);
    alert('An error occurred. Please try again later.');
  }
}



function onStudentEditModal(id) {
  isEditing = true;
  showStudentModal();
  const student = document.getElementById(`student-${id}`);
  if (!student) return;

  const cells = student.querySelectorAll("td");

  const group = cells[1].textContent;
  const fullName = cells[2].textContent.split(" ");
  const name = fullName[0];
  const surname = fullName[1];
  const gender = cells[3].textContent;
  const birthday = cells[4].textContent;

  // Populate form fields with student data
  document.getElementById("group").value = group;
  document.getElementById("name").value = name;
  document.getElementById("surname").value = surname;
  document.getElementById("gender").value = gender;
  document.getElementById("birthday").value = birthday;

  const modalTitle = document.getElementById("modal-title");
  modalTitle.textContent = "Edit Student";

  addStudentForm.removeEventListener("submit", onStudentEditModal);

  // Add event listener for form submission
  addStudentForm.onsubmit = async function (event) {
    event.preventDefault();

    if (isAdding) {
      return; 
    }

    const formData = new FormData(addStudentForm);
    formData.append('id', id); 

    const name = formData.get('name');
    const surname = formData.get('surname');
    const birthday = new Date(formData.get('birthday'));

    const nameValid = /^[A-Z][a-z]+$/.test(name);
    const surnameValid = /^[A-Z][a-z]+$/.test(surname);

    const currentDate = new Date();
    const age = currentDate.getFullYear() - birthday.getFullYear();
    const ageValid = age >= 16 && age <= 70;

    let errorMessage = "";
    if (!nameValid) errorMessage += "Name should start with an uppercase letter and consist of Latin letters.\n";
    if (!surnameValid) errorMessage += "Surname should start with an uppercase letter and consist of Latin letters.\n";
    if (!ageValid) errorMessage += "Age should be between 16 and 70 years.\n";

    if (errorMessage) {
      alert(errorMessage);
      return;
    }

    await updateStudent(id, formData);
  };

 
  closeModalButton.onclick = function (e) {
    e.preventDefault();
    hideStudentModal();
    isFormVisible = false;
    isEditing = false;
  };
}



function hideStudentModal() {
  addStudentModalWrapper.classList.add("hidden");
}

function showStudentModal() {
  addStudentModalWrapper.classList.remove("hidden");
}

function hideConfirmationModal() {
  confirmDeletionModalWrapper.classList.add("hidden");
}

function showConfirmationModal() {
  confirmDeletionModalWrapper.classList.remove("hidden");
}

document.addEventListener("mousedown", (e) => {
  if (isFormVisible) {
    handleClickOutside(e, addStudentModal, () => {
      if (!isFormVisible) return;
      hideStudentModal();
      isFormVisible = false;
    });
  }
  handleClickOutside(e, confirmDeletionModal, hideConfirmationModal);
});


function handleClickOutside({ target }, element, callback) {
  if (element && target instanceof HTMLElement && !element.contains(target)) {
    callback();
  }
}

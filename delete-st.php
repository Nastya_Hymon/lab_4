<?php
header("Content-Type: application/json");

// Перевірка методу запиту
if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    http_response_code(405); 
    echo json_encode(array(
        "success" => false,
        "message" => "Invalid request method."
    ));
    exit;
}

// Отримання ID студента, якого потрібно видалити
$id = $_POST["id"]; // Припускається, що ID передається в POST-запиті

// Підключення до бази даних
$servername = "127.0.0.1";
$username = "root";
$password = "root"; // Пароль до вашої бази даних (якщо є)
$dbname = "myfirstdb";

$conn = new mysqli($servername, $username, $password, $dbname);

// Перевірка з'єднання
if ($conn->connect_error) {
    http_response_code(500); 
    echo json_encode(array(
        "success" => false,
        "message" => "Database connection failed: " . $conn->connect_error
    ));
    exit;
}

// Видалення запису студента з бази даних
$sql = "DELETE FROM user WHERE id = '$id'";

if ($conn->query($sql) === TRUE) {
    echo json_encode(array(
        "success" => true,
        "message" => "Student deleted successfully."
    ));
} else {
    http_response_code(500); 
    echo json_encode(array(
        "success" => false,
        "message" => "Error deleting student: " . $conn->error
    ));
}

$conn->close();
?>

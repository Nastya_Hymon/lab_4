<?php
header("Content-Type: application/json");


function connectDB() {
    $servername = "localhost";
    $username = "root";
    $password = "94654"; 
    $dbname = "myfirstd";

    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        throw new Exception('Database connection failed: ' . $conn->connect_error);
    }
    return $conn;
}

function validateData($group, $name, $surname, $gender, $birthday) {
    $nameValid = preg_match('/^[A-Z][a-z]+$/', $name);
    $surnameValid = preg_match('/^[A-Z][a-z]+$/', $surname);

    $birthdayDate = new DateTime($birthday);
    $currentDate = new DateTime();
    $age = $birthdayDate->diff($currentDate)->y;
    $ageValid = ($age >= 16 && $age <= 70);

    if (!$nameValid || !$surnameValid || !$ageValid) {
        throw new Exception('Invalid data provided.');
    }
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $conn = connectDB();

    // Extract POST data
    $group = $_POST["group"];
    $name = $_POST["name"];
    $surname = $_POST["surname"];
    $gender = $_POST["gender"];
    $birthday = $_POST["birthday"];

    $id = isset($_POST["id"]) ? $_POST["id"] : null;

    try {
        
        validateData($group, $name, $surname, $gender, $birthday);

        if ($id !== null) {
            // Edit 
            $sql = "UPDATE user SET name='$name', surname='$surname', group_name='$group', gender='$gender', birthday='$birthday' WHERE id='$id'";
        } else {
            // Add 
            $sql = "INSERT INTO user (name, surname, group_name, gender, birthday) VALUES ('$name', '$surname', '$group', '$gender', '$birthday')";
        }

        if ($conn->query($sql) === TRUE) {
            $response = array("success" => true, "message" => ($id !== null ? "Student updated successfully." : "Student added successfully."));
        } else {
            throw new Exception('Error: ' . $conn->error);
        }
    } catch (Exception $e) {
        http_response_code(400); 
        $response = array("success" => false, "message" => "Error: " . $e->getMessage());
    }

    $conn->close();
    echo json_encode($response);
    exit;
}

// Delete 
if ($_SERVER["REQUEST_METHOD"] === "DELETE") {
    $deleteParams = json_decode(file_get_contents("php://input"), true);
    
    if (isset($deleteParams["id"])) {
        $id = $deleteParams["id"];
        
        error_log('Deleting student with ID: ' . $id);
        
        $conn = connectDB();
        
        try {
            $sql = "DELETE FROM user WHERE id='$id'";
            if ($conn->query($sql) === TRUE) {
                $response = array("success" => true, "message" => "Student deleted successfully.");
            } else {
                throw new Exception('Error: ' . $conn->error);
            }
        } catch (Exception $e) {
            http_response_code(500);
            $response = array("success" => false, "message" => "An error occurred: " . $e->getMessage());
        }
        
        $conn->close();
    } else {
        http_response_code(400);
        $response = array("success" => false, "message" => "Parameter 'id' is missing in the request.");
    }

    echo json_encode($response);
    exit;
}


http_response_code(405);
echo json_encode(array("success" => false, "message" => "Invalid request method."));
exit;
?>

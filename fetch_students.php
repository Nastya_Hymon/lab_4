<?php
header("Content-Type: application/json");


$servername = "localhost";
$username = "root";
$password = "94654"; 
$dbname = "myfirstd"; 

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM `user`"; 
$result = $conn->query($sql);

$students = [];

if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {
        $student = [
            'id' => $row['id'],
            'group' => $row['group_name'],
            'name' => $row['name'],
            'surname' => $row['surname'],
            'gender' => $row['gender'],
            'birthday' => $row['birthday'],
           
        ];
        $students[] = $student;
    }
}

echo json_encode(['success' => true, 'students' => $students]);

$conn->close();
?>

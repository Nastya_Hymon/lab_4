document.addEventListener('DOMContentLoaded', () => {
    const loginModal = document.getElementById('login-modal');
    const registerModal = document.getElementById('register-modal');
    const promptModal = document.getElementById('login-register-prompt');
    
    const loginCloseBtn = document.getElementById('login-close-btn');
    const registerCloseBtn = document.getElementById('register-close-btn');
    const openLoginModalBtn = document.getElementById('open-login-modal');
    const openRegisterModalBtn = document.getElementById('open-register-modal');
  
    // Show prompt modal on page load
    promptModal.classList.remove('hidden');
  
    // Show login modal
    openLoginModalBtn.addEventListener('click', () => {
      promptModal.classList.add('hidden');
      loginModal.classList.remove('hidden');
    });
  
    // Show register modal
    openRegisterModalBtn.addEventListener('click', () => {
      promptModal.classList.add('hidden');
      registerModal.classList.remove('hidden');
    });
  
    // Close login modal
    loginCloseBtn.addEventListener('click', () => {
      loginModal.classList.add('hidden');
    });
  
    // Close register modal
    registerCloseBtn.addEventListener('click', () => {
      registerModal.classList.add('hidden');
    });
  
    // Handle login form submission
    document.getElementById('login-form').addEventListener('submit', (e) => {
      e.preventDefault();
      // Handle login logic here
      alert('Login submitted');
      loginModal.classList.add('hidden');
    });
  
    // Handle register form submission
    document.getElementById('register-form').addEventListener('submit', (e) => {
      e.preventDefault();
      // Handle register logic here
      alert('Register submitted');
      registerModal.classList.add('hidden');
    });
  });
  